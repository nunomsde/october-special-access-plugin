<?php namespace Nmsde\Spa\Components;

use Rainlab\User\Models\User;
use DB;
use Auth;
use Model;
use Flash;
use Lang;
use Input;
use Nmsde\Spa\Models\UserInfo;
use System\Models\File;
use Log;

class SpecialAccess extends \Cms\Classes\ComponentBase
{

    public function componentDetails(){
        return [
            'name' => 'SpecialAccess',
            'description' => 'Display addition form for special access users'
        ];
    }

    public function init(){

        $component = $this->addComponent(
            'Nmsde\Spa\Components\FileUploader',
            'fileUploader',
            ['modelClass'=> new UserInfo , 'deferredBinding' => true , 'modelKeyColumn' => 'file']
        );

        $component->bindModel('file', new UserInfo);

    }

    public function onRun(){
        
        $this->addCss('assets/css/style.css');
        $this->addJs('assets/javascript/scripts.js');

    }

    public function onRefreshFiles(){
        $this->pageCycle();
    }

    public function onFileUpload(){

       $info = new UserInfo;

       $condition = post('condition');
       $transportation = post('transportation');
       $path =post('img_path');

       $user = Auth::getUser();

       $info->condition = $condition;
       $info->transportation = $transportation;
       $info->user_id = $user->id;
       $info->file_path = $path;
       $info->save(null, post('_session_key'));
    }
}