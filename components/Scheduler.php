<?php namespace Nmsde\Spa\Components;


use Nmsde\Spa\Models\Venue as Venues;
use Nmsde\Spa\Models\EventDay as Days;
use Nmsde\Spa\Models\VenueSlot as Slots;
use Nmsde\Spa\Models\Entrie;
use Rainlab\User\Models\User;
use DB;
use Auth;
use Model;
use Carbon\Carbon;
use Flash;
use Lang;

class Scheduler extends \Cms\Classes\ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Scheduler',
            'description' => 'Display Form'
        ];
    }

    public function Days(){
        return Days::loadDays();
    }
    public function Venues(){
        return Venues::loadVenues();
    }

    public function onRun(){

        $this->addCss('/plugins/nmsde/spa/assets/css/ickeck.css');
        $this->addCss('/plugins/nmsde/spa/assets/css/style.css');

        $this->addJs('/plugins/nmsde/spa/assets/javascript/scripts.js');
        $this->addJs('/plugins/nmsde/spa/assets/javascript/icheck.min.js');

    }

    function onSelectVenue(){

        $day = post('day');
        $venue = post('venue');

        if (empty($day)){
            Flash::error(Lang::get('nmsde.spa::lang.day_error'));
            return ['#messages' => $this->renderPartial('scheduler::flash_message_partial')];
        }

        $user = Auth::getUser();
        $this->page['result'] = [ 
                'slots' => Slots::eventDay($day)->venue($venue)->available()->notScheduledByUser($user->id)->get(),
                'day' => $day,
                'venue' => $venue
        ];
    }

    function onSubmit(){

        $day = post('day');
        $venue = post('venue');
        $slots  = post('slot');

        if(empty($slots)){
            Flash::error(Lang::get('nmsde.spa::lang.slots_error'));
            return['#messages' => $this->renderPartial('scheduler::flash_message_partial')];
        }

        $user = Auth::getUser();

        foreach ($slots as $slot) {
            $entrie = Entrie::firstOrCreate(['user_id'=> $user['id'] , 'event_day_id' => $day , 'venue_id' => $venue , 'venue_slot_id' => $slot]);
        }

        Flash::success('nmsde.spa::lang.slots_success');
        return ['#messages' => $this->renderPartial('scheduler::flash_message_partial')]; 
    }
}