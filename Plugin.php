<?php namespace nmsde\spa;

use System\Classes\PluginBase;
use Rainlab\User\Models\User;
use Nmsde\Spa\Models\UserInfo;
use Event;
use Log;

class Plugin extends PluginBase{

    public function registerComponents(){
    	return [
        	'Nmsde\Spa\Components\Scheduler' => 'scheduler',
            'Nmsde\Spa\Components\Specialaccess' => 'specialaccess',
            'Nmsde\Spa\Components\FileUploader'  => 'fileUploader'
    	];
    }

    public function registerMailTemplates(){
	    return [
	        'nmsde.spa::mail.special_access_approved' => 'Send special access approved.',
	        'nmsde.spa::mail.special_acccess_not_approved'  => 'Send special access request not acepted.',
            'nmsde.spa::mail.special_access_new_entrie' => 'Send email to admin'
	    ];
	}

    public function boot(){

        User::extend(function($model) {

            // relation to add fields to rainlab\user\models\user form 
            $model->hasOne['userinfo'] = ['Nmsde\Spa\Models\UserInfo'];

            // relation to add fiels to nmsde\spa\models\entrie form
            $model->hasOne['condition'] = ['Nmsde\Spa\Models\UserInfo'];
            $model->hasOne['transportation'] = ['Nmsde\Spa\Models\UserInfo'];
            $model->hasOne['file'] = ['Nmsde\Spa\Models\UserInfo'];

        });

        // add fields to backend using event
        Event::listen('backend.form.extendFields', function($widget) {
    
            if (!$widget->getController() instanceof \RainLab\User\Controllers\Users) return;

            //if(!UserInfo::hasUser($widget->model)) return;

            if(!$widget->model->exists) return;
            
            if (!UserInfo::getFromUser($widget->model)) return;
          
            if ($widget->getContext() != 'update') return;
            
            $widget->addTabFields([
                'userinfo[transportation]'=> [
                    'label' => 'Transportation',
                    'tab' => 'Special Access Info',
                    'comment' => '',
                    'type' => 'text'
                ],
                'userinfo[condition]' => [
                    'label' => 'Condition',
                    'tab' => 'Special Access Info',
                    'comment' => '',
                    'type' => 'text'
                ],
                'userinfo[file]' => [
                    'label' => 'File',
                    'tab' => 'Special Access Info',
                    'comment' => 'Display uploaded file.',
                    'type' => 'fileupload'
                ]
            ], 'primary');

        });
    }

   public function registerSettings(){
        return [
            'settings' => [
                'label'       => 'Settings',
                'description' => 'Manage special access based settings.',
                'category'    => 'Special Access',
                'icon'        => 'icon-cog',
                'class'       => 'Nmsde\Spa\Models\Settings',
                'order'       => 500,
                'keywords'    => 'special access settigns',
                'permissions' => ['nmsde.spa.settings_permission']
            ]
        ];
    }
}
