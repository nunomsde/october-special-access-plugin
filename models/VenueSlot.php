<?php namespace nmsde\spa\Models;

use Model;
use Nmsde\Spa\Models\Entrie;
use DB;
/**
 * Model
 */
class VenueSlot extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $belongsTo = [
        'venue' => 'nmsde\spa\Models\Venue',
        'event_day' => 'nmsde\spa\Models\EventDay'
    ];

    /*
     * Validation
     */
    public $rules = [
    ];

    public $dates = [];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nmsde_spa_venue_slot';


    public function setStartTimeAttribute($value)
    {
        $this->attributes['start_time'] = date('Y-m-d H:i:s', strtotime($value));
    }

    public function setEndTimeAttribute($value)
    {
        $this->attributes['end_time'] = date('Y-m-d H:i:s', strtotime($value));
        
    }

    public function afterFetch(){
        
        $this->start_time = date('Y-m-d H:i:s', strtotime('0-0-0 ' . $this->start_time));
        $this->end_time = date('Y-m-d H:i:s', strtotime('0-0-0 ' . $this->end_time));

        $this->dates[] = 'start_time';
        $this->dates[] = 'end_time';

    }

    public function scopeAvailable($query){
        return $query->where('total_available_seats','>','total_filled_seats');
    } 

    public function scopeEventDay($query , $day){
        return $query->where('event_day_id','=',$day);
    }

    public function scopeVenue($query , $venue){
        return $query->where('venue_id','=',$venue);
    }

    public function scopeSlot($query , $slot){
        return $query->where('id','=',$slot);
    }

    public function scopeIncrementSeats($query){
        return $query->increment('total_filled_seats',1);
    }

    public function scopeDecrementSeats($query){
        return $query->decrement('total_filled_seats', 1);
    }

    public function scopeNotScheduledByUser($query , $user){
        $entries = [];
        $resutls = Entrie::isUser($user);
        foreach ($resutls as $result) {
            $entries[] = $result;
        }
        return $query->whereNotIn('id', $entries);
    }
    
}