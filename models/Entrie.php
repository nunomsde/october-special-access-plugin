<?php namespace nmsde\spa\Models;

use Model;
use DB;
use Nmsde\Spa\Models\VenueSlot as Slots;
use Nmsde\Spa\Models\UserInfo;
use Mail;
use Lang;
use Rainlab\User\Models\User;
use Nmsde\spa\Models\Settings;
/**
 * Model
 */
class Entrie extends Model
{
    use \October\Rain\Database\Traits\Validation;


    public $belongsTo = [
        'venue' => 'Nmsde\Spa\Models\Venue',
        'event_day' => 'Nmsde\Spa\Models\EventDay',
        'venue_slot' => 'Nmsde\Spa\Models\VenueSlot',
        'user' => 'Rainlab\User\Models\User'
    ];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','venue_id','event_day_id','venue_slot_id'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nmsde_spa_entrie';

    public function scopeIsUser($query , $user){
        return $query->where('user_id','=',$user)->lists('venue_slot_id');
    }

    // function will decrease seats filled before delete entrie.
    public function beforeDelete(){
        Slots::slot($this->venue_slot_id)->decrementSeats();
    }

    // function send email to management email when a new entrie is created
    public function afterCreate(){
        sendEmail('admin');
    }

    public function setApprovedAttribute($value){
        if($value === 1){
            // function will increase seats filled after create entrie
            Slots::slot($this->venue_slot_id)->incrementSeats();
            // send email to client confiming that entrie was approved
            sendEmail('approved');
        }
    }

    // Send email when approved attribute is set
    public function sendEmail($type){
        $user = User::find($this->user_id);
        $slot = Slots::find($this->venue_slot_id);

        $managementEmail = Settings::get('management_email');

        $vars = ['name' => $user->name , 'concert' => $slot->name];

        if($type == 'approved'){
            Mail::send('nmsde.spa::mail.special_access_approved', $vars, function($message) {
                $message->to($user->email , 'Client');
                $message->subject(Lang::get('nmsde.spa::lang.client_seat_approved_mail_subject'));
            });
        }else if($type == 'admin'){
            Mail::send('nmsde.spa::mail.special_access_new_entrie', $vars, function($message) {
                $message->to($managementEmail , 'Admin');
                $message->subject(Lang::get('nmsde.spa::lang.new_entrie'));
            });
        }else if($type == 'notapproved'){
            Mail::send('nmsde.spa::mail.special_acccess_not_approved', $vars, function($message) {
                $message->to($user->email , 'Client');
                $message->subject(Lang::get('nmsde.spa::lang.client_seat_approved_mail_subject'));
            });
        }
    }
}