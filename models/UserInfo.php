<?php namespace nmsde\spa\Models;

use Model;
use DB;
/**
 * Model
 */
class UserInfo extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $belongsTo = [
        'user' => ['Rainlab\User\Models\User' ,'foreignKey' => 'user_id']
    ];


    public $attachOne = [
        'file' => ['System\Models\File']
    ];

    /*
     * Validation
     */
    public $rules = [
    ]; 

    protected $fillable = ['condition', 'transportation', 'user_id', 'file', 'file_path'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nmsde_spa_user_info';

    public static function getFromUser($user){
        
        if($user->userinfo)
            return $user->userinfo;

        $userinfo = new static;
        $userinfo->user = $user;
        $userinfo->save();

        $user->userinfo = $userinfo;

        return $userinfo;
    }
    public function getFilePathAttribute(){
        return '<img src="'.$this->fill.'" height="200" width="300" style="margin-top:50px" />';
    }

    // verify if user requested special access
    public function scopeHasUser($query, $user){
        return $query->where('user_id','=',$user->id)->get() ? true : false;
    }

}