<?php namespace nmsde\spa\Models;

use Model;
use DB;
/**
 * Model
 */
class Venue extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nmsde_spa_venue';

    public function scopeLoadVenues(){
        return DB::table($this->table)->get();
    }
}