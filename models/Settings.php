<?php namespace nmsde\spa\Models;

use Model;

/**
 * Model
 */
class Settings extends Model
{

    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'nmsde_spa_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

}