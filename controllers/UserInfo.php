<?php namespace nmsde\spa\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class UserInfo extends Controller
{
    public $implement = ['Backend\Behaviors\FormController'];
    
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'user_info_permission' 
    ];

    public function __construct()
    {
        parent::__construct();
    }
}