<?php namespace nmsde\spa\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class SettingsController extends Controller
{
    public $implement = ['Backend\Behaviors\FormController'];
    
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'settings_permission' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('nmsde.spa', 'main-menu-item', 'side-menu-item5');
    }
}