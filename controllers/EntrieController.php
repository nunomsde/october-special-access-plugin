<?php namespace nmsde\spa\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class EntrieController extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController','Backend.Behaviors.RelationController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = [
        'entrie_permission' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('nmsde.spa', 'main-menu-item', 'side-menu-item');
    }
}