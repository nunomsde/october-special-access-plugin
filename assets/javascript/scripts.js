// submit for when a vaneu is select 
$('[id*="venue-"]').on('ifChecked',function(){
    $('#slots-form').submit();
});

// load icheck on document ready
$(document).ready(function(){

  $('input').each(function(){
    var self = $(this),
      label = self.next(),
      label_text = label.text();

    label.remove();
    self.iCheck({
      checkboxClass: 'icheckbox_line-blue',
      radioClass: 'iradio_line-blue',
      insert: '<div class="icheck_line-icon"></div>' + label_text
    });
  });
});

// lead icheck on ajaxupdate
$('#result').on('ajaxUpdate', function() {
    
    $('#result :input').each(function(){
		  var self = $(this),
		      label = self.next(),
		      label_text = label.text();

		    label.remove();
		    self.iCheck({
		      checkboxClass: 'icheckbox_line-blue',
		      radioClass: 'iradio_line-blue',
		      insert: '<div class="icheck_line-icon"></div>' + label_text
		 });
	});
});

