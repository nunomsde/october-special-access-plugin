<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNmsdeSpaEventDayVenue extends Migration
{
    public function up()
    {
        Schema::table('nmsde_spa_event_day_venue', function($table)
        {
            $table->increments('id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('nmsde_spa_event_day_venue', function($table)
        {
            $table->dropColumn('id');
        });
    }
}
