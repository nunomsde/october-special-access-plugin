<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNmsdeSpaUserInfo2 extends Migration
{
    public function up()
    {
        Schema::table('nmsde_spa_user_info', function($table)
        {
            $table->renameColumn('file', 'files');
        });
    }
    
    public function down()
    {
        Schema::table('nmsde_spa_user_info', function($table)
        {
            $table->renameColumn('files', 'file');
        });
    }
}
