<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNmsdeSpaUserInfo5 extends Migration
{
    public function up()
    {
        Schema::table('nmsde_spa_user_info', function($table)
        {
            $table->string('file_path')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('nmsde_spa_user_info', function($table)
        {
            $table->dropColumn('file_path');
        });
    }
}
