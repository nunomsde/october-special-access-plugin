<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNmsdeSpaUserInfo6 extends Migration
{
    public function up()
    {
        Schema::table('nmsde_spa_user_info', function($table)
        {
            $table->string('fill')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('nmsde_spa_user_info', function($table)
        {
            $table->dropColumn('fill');
        });
    }
}
