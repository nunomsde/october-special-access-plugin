<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNmsdeSpaUserInfo extends Migration
{
    public function up()
    {
        Schema::create('nmsde_spa_user_info', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id');
            $table->string('condition', 255)->nullable();
            $table->string('transportation', 255)->nullable();
            $table->string('file', 500)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nmsde_spa_user_info');
    }
}
