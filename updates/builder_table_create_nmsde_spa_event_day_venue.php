<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNmsdeSpaEventDayVenue extends Migration
{
    public function up()
    {
        Schema::create('nmsde_spa_event_day_venue', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('venue_id');
            $table->integer('event_day_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nmsde_spa_event_day_venue');
    }
}
