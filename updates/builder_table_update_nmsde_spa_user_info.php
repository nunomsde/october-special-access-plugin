<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNmsdeSpaUserInfo extends Migration
{
    public function up()
    {
        Schema::table('nmsde_spa_user_info', function($table)
        {
            $table->string('file')->change();
        });
    }
    
    public function down()
    {
        Schema::table('nmsde_spa_user_info', function($table)
        {
            $table->string('file', 500)->change();
        });
    }
}
