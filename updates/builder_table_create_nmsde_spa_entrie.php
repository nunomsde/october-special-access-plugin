<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNmsdeSpaEntrie extends Migration
{
    public function up()
    {
        Schema::create('nmsde_spa_entrie', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->nullable();
            $table->integer('event_day_id')->nullable();
            $table->integer('venue_slot_id')->nullable();
            $table->integer('venue_id')->nullable();
            $table->boolean('approved')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nmsde_spa_entrie');
    }
}
