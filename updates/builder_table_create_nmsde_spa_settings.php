<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNmsdeSpaSettings extends Migration
{
    public function up()
    {
        Schema::create('nmsde_spa_settings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('management_email')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nmsde_spa_settings');
    }
}
