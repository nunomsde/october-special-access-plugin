<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNmsdeSpaVenueSlot2 extends Migration
{
    public function up()
    {
        Schema::table('nmsde_spa_venue_slot', function($table)
        {
            $table->integer('venue_id');
            $table->integer('event_day_id');
        });
    }
    
    public function down()
    {
        Schema::table('nmsde_spa_venue_slot', function($table)
        {
            $table->dropColumn('venue_id');
            $table->dropColumn('event_day_id');
        });
    }
}
