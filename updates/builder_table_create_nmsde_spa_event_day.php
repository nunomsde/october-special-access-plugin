<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNmsdeSpaEventDay extends Migration
{
    public function up()
    {
        Schema::create('nmsde_spa_event_day', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('day');
            $table->date('date');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nmsde_spa_event_day');
    }
}
