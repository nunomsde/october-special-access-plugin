<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNmsdeSpaVenueSlot extends Migration
{
    public function up()
    {
        Schema::table('nmsde_spa_venue_slot', function($table)
        {
            $table->dropColumn('event_day_venue_id');
        });
    }
    
    public function down()
    {
        Schema::table('nmsde_spa_venue_slot', function($table)
        {
            $table->integer('event_day_venue_id');
        });
    }
}
