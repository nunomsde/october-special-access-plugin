<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNmsdeSpaVenueSlot extends Migration
{
    public function up()
    {
        Schema::create('nmsde_spa_venue_slot', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 255)->nullable();
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->integer('total_available_seats')->nullable()->default(0);
            $table->integer('total_filled_seats')->nullable()->default(0);
            $table->integer('event_day_venue_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nmsde_spa_venue_slot');
    }
}
