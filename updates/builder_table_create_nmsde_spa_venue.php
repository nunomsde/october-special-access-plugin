<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNmsdeSpaVenue extends Migration
{
    public function up()
    {
        Schema::create('nmsde_spa_venue', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nmsde_spa_venue');
    }
}
