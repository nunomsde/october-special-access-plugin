<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteNmsdeSpaEventDayVenue extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nmsde_spa_event_day_venue');
    }
    
    public function down()
    {
        Schema::create('nmsde_spa_event_day_venue', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('venue_id');
            $table->integer('event_day_id');
        });
    }
}
