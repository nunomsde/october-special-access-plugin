<?php namespace nmsde\spa\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNmsdeSpaVenueSlot3 extends Migration
{
    public function up()
    {
        Schema::table('nmsde_spa_venue_slot', function($table)
        {
            $table->dateTime('start_time')->nullable()->unsigned(false)->default(null)->change();
            $table->dateTime('end_time')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('nmsde_spa_venue_slot', function($table)
        {
            $table->time('start_time')->nullable()->unsigned(false)->default(null)->change();
            $table->time('end_time')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
